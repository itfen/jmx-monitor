package com.unitfen.jmx.mxbean.creator;


import javax.management.MBeanServerConnection;
import java.lang.management.MemoryPoolMXBean;
import java.util.ArrayList;
import java.util.List;
public class MemoryPoolMXBeanCreator extends MXBeanCreator{
    private static final String[] beanNames= new String[] {
            "Code Cache","Compressed Class Space","Metaspace","PS Eden Space","PS Old Gen","PS Survivor Space"};

    @Override
    public Class<?> getMXBeanClass() {
        return  MemoryPoolMXBean.class;
    }

    @Override
    String getMxbeanName() {
        return  "java.lang:type=MemoryPool";
    }

   public List<MemoryPoolMXBean> getMemoryPoolList(MBeanServerConnection mbsc){
        List<MemoryPoolMXBean> back= new ArrayList<MemoryPoolMXBean>();
        for (String key:beanNames){
            MemoryPoolMXBean entity =super.MXBeanCreator(mbsc, getMxbeanName() + ",name=" + key);
            back.add(entity);
        }
        return back;
    }




}
