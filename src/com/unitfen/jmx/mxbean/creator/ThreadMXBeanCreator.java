package com.unitfen.jmx.mxbean.creator;


import java.lang.management.ThreadMXBean;
public class ThreadMXBeanCreator extends MXBeanCreator {



    @Override
     public Class<?> getMXBeanClass() {
        return ThreadMXBean.class;
    }

    @Override
    String getMxbeanName() {
        return "java.lang:type=Threading";
    }


}
