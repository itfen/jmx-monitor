package com.unitfen.jmx.mxbean.creator;

import java.lang.management.MemoryMXBean;

public class MemoryMXBeanCreator extends MXBeanCreator{
    @Override
    public Class<?> getMXBeanClass() {
        return  MemoryMXBean.class;
    }

    @Override
    String getMxbeanName() {
        return  "java.lang:type=Memory";
    }
}
