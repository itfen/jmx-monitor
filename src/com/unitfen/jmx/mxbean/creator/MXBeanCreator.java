package com.unitfen.jmx.mxbean.creator;

import javax.management.MBeanServerConnection;
import java.io.IOException;
import java.lang.management.ManagementFactory;

public abstract class  MXBeanCreator  {

    protected abstract  Class<?> getMXBeanClass();
    abstract String getMxbeanName();
    public   <T> T MXBeanCreator(MBeanServerConnection mbsc) {


        return MXBeanCreator(mbsc,null);
    }
    public   <T> T MXBeanCreator(MBeanServerConnection mbsc,String mxBeanName) {
                if(mxBeanName==null){
                    mxBeanName=getMxbeanName();
                }
        try {
            return  ManagementFactory.<T>newPlatformMXBeanProxy(mbsc,mxBeanName, (Class<T>) getMXBeanClass());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
