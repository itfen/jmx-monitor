package com.unitfen.jmx.client.impl;

import com.unitfen.jmx.client.entity.MemoryUsageEntity;
import com.unitfen.jmx.client.entity.ThreadEntity;
import com.unitfen.jmx.mxbean.creator.MemoryMXBeanCreator;
import com.unitfen.jmx.mxbean.creator.MemoryPoolMXBeanCreator;
import com.unitfen.jmx.mxbean.creator.ThreadMXBeanCreator;

import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.ThreadMXBean;
import java.util.List;

/**
 *这个是使用MXbean的主要用来连接远程的jmx服务且获取进程信息和内存使用信息
 *
 */
public class MXBeanServiceTest {

    private  Integer pid;


    public MXBeanServiceTest(Integer pid) {
        this.pid = pid;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public ThreadEntity getThteadCount(){
        JMXConnectorService jmxc = JMXConnectorService.getInstance(pid);
        try {
            ThreadMXBean threadInfo = jmxc.MXBeanCreator(new ThreadMXBeanCreator());
          return  new ThreadEntity(threadInfo);
        }catch (Exception es){
            return null;
        }
    }
    public MemoryUsageEntity getMemoryUsageCount(){
        JMXConnectorService jmxc = JMXConnectorService.getInstance(pid);
        try {
            MemoryMXBean memoryMXBean = jmxc.MXBeanCreator(new MemoryMXBeanCreator());
           return  new MemoryUsageEntity(  memoryMXBean.getHeapMemoryUsage(), memoryMXBean.getNonHeapMemoryUsage());
        }catch (Exception es){
            return null;
        }
    }


    public   List<MemoryPoolMXBean> getMemoryPoolMXBean(){
        JMXConnectorService jmxc = JMXConnectorService.getInstance(pid);
        try {
          return new MemoryPoolMXBeanCreator().getMemoryPoolList(jmxc.getJMXConnector().getMBeanServerConnection());
        }catch (Exception es){
            return null;
        }
    }




    
}
