package com.unitfen.jmx.client.impl;

import com.sun.tools.attach.AgentInitializationException;
import com.sun.tools.attach.AgentLoadException;
import com.sun.tools.attach.AttachNotSupportedException;
import com.sun.tools.attach.VirtualMachine;
import com.sun.tools.hat.internal.model.Snapshot;
import com.unitfen.jmx.client.entity.LocalVirtualMachine;
import com.unitfen.jmx.mxbean.creator.MXBeanCreator;
import sun.jvmstat.monitor.*;
import sun.management.ConnectorAddressLink;
import sun.tools.attach.HotSpotVirtualMachine;

import javax.management.*;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


/**
 * Jmx服务连接  单例
 */
public final class  JMXConnectorService {

    private SnapshotMBeanServerConnection server = null;
    private   MBeanServer mbsc;
    private   HotSpotVirtualMachine virtualMachine  ;
    private   String jmxPort;
    private   JMXConnector jmxc;
    private   Integer pid=0;
    private volatile static JMXConnectorService instance =null;

    private JMXConnectorService( ){

    };
    private JMXConnectorService(Integer pid){
        connectLocalHostByPid(pid);
    };



    //等到需要使用时进行创建
    public static JMXConnectorService getInstance(Integer newPid ){

        if (instance==null ){
            synchronized (JMXConnectorService.class){
                if (instance==null) instance=new JMXConnectorService(newPid);
            }
        }else if (!instance.pid.equals( newPid)){
            synchronized (JMXConnectorService.class){
                instance=new JMXConnectorService(newPid);
            }
        }

        return instance;
    }





    /**
     * 连接本地的指定进程id的虚拟机并获取 JMXConnector
     *
     * @param pid
     * @return
     */

    public JMXConnector connectLocalHostByPid(Integer pid) {
        doBefor();
       this.pid=pid;

        try {

            LocalVirtualMachine tempLvm = LocalVirtualMachineService.getLocalVirtualMachine(pid);


            if(tempLvm!=null){
                loadManagementAgent(tempLvm);
             /*   String str = "/jndi/rmi://localhost:" + jmxPort + "/jmxrmi";
                JMXServiceURL jMXServiceURL = new JMXServiceURL("rmi", "", 0, str);
                jmxc = JMXConnectorFactory.connect(jMXServiceURL, null);*/
                JMXServiceURL jmxUrl = new JMXServiceURL(tempLvm.getAddress());
                this.jmxc = JMXConnectorFactory.connect(jmxUrl);
            }else{
                throw new Exception("连接进程id号为 "+pid+" 的Java.exe 进程失败 ");
            }

        }catch (Exception es){
            es.printStackTrace();
        }
        return jmxc;
    }





    private void getLocalHostJmxPort  () throws Exception {
        try {
            VirtualMachine vm = VirtualMachine.attach(pid.toString());
            virtualMachine = (HotSpotVirtualMachine) vm;
            jmxPort = virtualMachine.getSystemProperties().getProperty("com.sun.management.jmxremote.port");
        }catch (Exception es){
            jmxPort=null;
            throw new Exception("连接进程id号为 "+pid+" 的Java.exe 进程失败 ");

        }
    }

    public void initMbsc(){
        this.mbsc = ManagementFactory.getPlatformMBeanServer();
        this.server=Snapshot.newSnapshot(mbsc);
    }













    /**
     * 关掉虚拟机连接
     *
     *
     */

    public void closeConnection() throws Exception{
        if(this.jmxc==null){
            return ;
        }
        jmxc.close();

    }

    public void doBefor(){

        try{
            closeConnection();
        }catch (Exception es){

        }
        LocalVirtualMachineService.getMonitoredVMs();
        initMbsc();
        virtualMachine=null;
        jmxPort=null;
        jmxc=null;
    }

     public <T> T MXBeanCreator(MXBeanCreator mxBeanCreator) throws IOException {

       return  (T) mxBeanCreator.MXBeanCreator(jmxc.getMBeanServerConnection());
     }


     public JMXConnector getJMXConnector(){
        return jmxc;
     }




    public interface SnapshotMBeanServerConnection
            extends MBeanServerConnection {
        /**
         * Flush all cached values of attributes.
         */
        public void flush();
    }

    public static class Snapshot {
        private Snapshot() {
        }
        public static SnapshotMBeanServerConnection
        newSnapshot(MBeanServerConnection mbsc) {
            final InvocationHandler ih = new SnapshotInvocationHandler(mbsc);
            return (SnapshotMBeanServerConnection) Proxy.newProxyInstance(
                    Snapshot.class.getClassLoader(),
                    new Class[] {SnapshotMBeanServerConnection.class},
                    ih);
        }
    }

    static class SnapshotInvocationHandler implements InvocationHandler {

        private final MBeanServerConnection conn;
        private Map<ObjectName, NameValueMap> cachedValues = newMap();
        private Map<ObjectName, Set<String>> cachedNames = newMap();

        @SuppressWarnings("serial")
        private static final class NameValueMap
                extends HashMap<String, Object> {}

        SnapshotInvocationHandler(MBeanServerConnection conn) {
            this.conn = conn;
        }

        synchronized void flush() {
            cachedValues = newMap();
        }

        public Object invoke(Object proxy, Method method, Object[] args)
                throws Throwable {
            final String methodName = method.getName();
            if (methodName.equals("getAttribute")) {
                return getAttribute((ObjectName) args[0], (String) args[1]);
            } else if (methodName.equals("getAttributes")) {
                return getAttributes((ObjectName) args[0], (String[]) args[1]);
            } else if (methodName.equals("flush")) {
                flush();
                return null;
            } else {
                try {
                    return method.invoke(conn, args);
                } catch (InvocationTargetException e) {
                    throw e.getCause();
                }
            }
        }

        private Object getAttribute(ObjectName objName, String attrName)
                throws MBeanException, InstanceNotFoundException,
                AttributeNotFoundException, ReflectionException, IOException {
            final NameValueMap values = getCachedAttributes(
                    objName, Collections.singleton(attrName));
            Object value = values.get(attrName);
            if (value != null || values.containsKey(attrName)) {
                return value;
            }
            // Not in cache, presumably because it was omitted from the
            // getAttributes result because of an exception.  Following
            // call will probably provoke the same exception.
            return conn.getAttribute(objName, attrName);
        }

        private AttributeList getAttributes(
                ObjectName objName, String[] attrNames) throws
                InstanceNotFoundException, ReflectionException, IOException {
            final NameValueMap values = getCachedAttributes(
                    objName,
                    new TreeSet<String>(Arrays.asList(attrNames)));
            final AttributeList list = new AttributeList();
            for (String attrName : attrNames) {
                final Object value = values.get(attrName);
                if (value != null || values.containsKey(attrName)) {
                    list.add(new Attribute(attrName, value));
                }
            }
            return list;
        }

        private synchronized NameValueMap getCachedAttributes(
                ObjectName objName, Set<String> attrNames) throws
                InstanceNotFoundException, ReflectionException, IOException {
            NameValueMap values = cachedValues.get(objName);
            if (values != null && values.keySet().containsAll(attrNames)) {
                return values;
            }
            attrNames = new TreeSet<String>(attrNames);
            Set<String> oldNames = cachedNames.get(objName);
            if (oldNames != null) {
                attrNames.addAll(oldNames);
            }
            values = new NameValueMap();
            final AttributeList attrs = conn.getAttributes(
                    objName,
                    attrNames.toArray(new String[attrNames.size()]));
            for (Attribute attr : attrs.asList()) {
                values.put(attr.getName(), attr.getValue());
            }
            cachedValues.put(objName, values);
            cachedNames.put(objName, attrNames);
            return values;
        }

        // See http://www.artima.com/weblogs/viewpost.jsp?thread=79394
        private static <K, V> Map<K, V> newMap() {
            return new HashMap<K, V>();
        }
    }


    private void loadManagementAgent(LocalVirtualMachine lvm) throws IOException {
        VirtualMachine virtualMachine = null;
        String str1 = String.valueOf(lvm.getVmid());
        try {
            virtualMachine = VirtualMachine.attach(str1);
        } catch (AttachNotSupportedException attachNotSupportedException) {
            IOException iOException = new IOException(attachNotSupportedException.getMessage());
            iOException.initCause((Throwable)attachNotSupportedException);
            throw iOException;
        }
        String str2 = virtualMachine.getSystemProperties().getProperty("java.home");
        String str3 = str2 + File.separator + "jre" + File.separator + "lib" + File.separator + "management-agent.jar";
        File file = new File(str3);
        if (!file.exists()) {
            str3 = str2 + File.separator + "lib" + File.separator + "management-agent.jar";
            file = new File(str3);
            if (!file.exists())
                throw new IOException("Management agent not found");
        }
        str3 = file.getCanonicalPath();
        try {
            virtualMachine.loadAgent(str3, "com.sun.management.jmxremote");
        } catch (AgentLoadException agentLoadException) {
            IOException iOException = new IOException(agentLoadException.getMessage());
            iOException.initCause((Throwable)agentLoadException);
            throw iOException;
        } catch (AgentInitializationException agentInitializationException) {
            IOException iOException = new IOException(agentInitializationException.getMessage());
            iOException.initCause((Throwable)agentInitializationException);
            throw iOException;
        }
        Properties properties = virtualMachine.getAgentProperties();
        lvm.setAddress((String) properties.get("com.sun.management.jmxremote.localConnectorAddress"));
        virtualMachine.detach();
    }



}
