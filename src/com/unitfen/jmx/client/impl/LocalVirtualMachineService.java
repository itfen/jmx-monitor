package com.unitfen.jmx.client.impl;

import com.unitfen.jmx.client.entity.LocalVirtualMachine;
import sun.jvmstat.monitor.*;
import sun.management.ConnectorAddressLink;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class LocalVirtualMachineService {
    //用来保存本地的全部在运行的java进程中JVM的信息
    private static volatile Map<Integer, LocalVirtualMachine> lvmMap=new ConcurrentHashMap<Integer, LocalVirtualMachine>();

    public    static  Set<Integer> getAllLocalHostPid(){
        getMonitoredVMs();
        return lvmMap.keySet();
    }

    public  static    LocalVirtualMachine getLocalVirtualMachine(Integer pid){
        if( lvmMap==null|| lvmMap.size()==0){
            return null;
        }else{
            if( lvmMap.containsKey(pid)){
                return lvmMap.get(pid);
            }
        }
        return null;
    }


    public   static  void getMonitoredVMs() {
        MonitoredHost host;
        Set vms;
        try {
            host = MonitoredHost.getMonitoredHost(new HostIdentifier((String)null));
            vms = host.activeVms();
        } catch (java.net.URISyntaxException sx) {
            throw new InternalError(sx.getMessage());
        } catch (MonitorException mx) {
            throw new InternalError(mx.getMessage());
        }
        for (Object vmid: vms) {
            if (vmid instanceof Integer) {
                int pid = ((Integer) vmid).intValue();
                String name = vmid.toString(); // default to pid if name not available
                boolean attachable = false;
                String address = null;
                try {
                    MonitoredVm mvm = host.getMonitoredVm(new VmIdentifier(name));
                    // use the command line as the display name
                    name =  MonitoredVmUtil.commandLine(mvm);
                    attachable = MonitoredVmUtil.isAttachable(mvm);
                    address = ConnectorAddressLink.importFrom(pid);
                    mvm.detach();
                } catch (Exception x) {
                    // ignore
                }
                lvmMap.put((Integer) vmid,
                        new LocalVirtualMachine(pid, name, attachable, address));
            }
        }
    }

}
