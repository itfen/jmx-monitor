package com.unitfen.jmx.client.entity;

import java.lang.management.ThreadMXBean;

/**
 * 线程数量
 */
public class ThreadEntity {
    private Integer peakThreadCount;
    private Integer threadCount;
    private Integer daemonThreadCount;
    private Long totalStartedThreadCount;

    public ThreadEntity() {

    }
    public ThreadEntity( ThreadMXBean t) {
        this.peakThreadCount=t.getPeakThreadCount();
        this.threadCount=t.getThreadCount();
        this.daemonThreadCount=t.getDaemonThreadCount();
        this.totalStartedThreadCount=t.getTotalStartedThreadCount();
    }



    public Integer getPeakThreadCount() {
        return peakThreadCount;
    }

    public void setPeakThreadCount(Integer peakThreadCount) {
        this.peakThreadCount = peakThreadCount;
    }

    public Integer getThreadCount() {
        return threadCount;
    }

    public void setThreadCount(Integer threadCount) {
        this.threadCount = threadCount;
    }

    public Integer getDaemonThreadCount() {
        return daemonThreadCount;
    }

    public void setDaemonThreadCount(Integer daemonThreadCount) {
        this.daemonThreadCount = daemonThreadCount;
    }

    public Long getTotalStartedThreadCount() {
        return totalStartedThreadCount;
    }

    public void setTotalStartedThreadCount(Long totalStartedThreadCount) {
        this.totalStartedThreadCount = totalStartedThreadCount;
    }

    @Override
    public String toString() {
        return "ThreadEntity{" +
                "peakThreadCount=" + peakThreadCount +
                ", threadCount=" + threadCount +
                ", daemonThreadCount=" + daemonThreadCount +
                ", totalStartedThreadCount=" + totalStartedThreadCount +
                '}';
    }
}
