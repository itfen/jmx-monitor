package com.unitfen.jmx.client.entity;

import java.lang.management.MemoryUsage;

/**
 * 用于读取内存使用信息 有为堆内容和非堆内存
 *
 */
public class MemoryUsageEntity {

    private   long heapInit;
    private   long heapUsed;
    private   long heapCommitted;
    private   long heapMax;

    private   long nonHeapInit;
    private   long nonHeapUsed;
    private   long nonHeapCommitted;
    private   long nonHeapMax;




    public MemoryUsageEntity(){}

    public MemoryUsageEntity(MemoryUsage heap, MemoryUsage nonHeap){
        this.heapInit=heap.getInit();
        this.heapUsed=heap.getUsed();
        this.heapCommitted=heap.getCommitted();
        this.heapMax=heap.getMax();

        this.nonHeapInit=nonHeap.getInit();
        this.nonHeapUsed=nonHeap.getUsed();
        this.nonHeapCommitted=nonHeap.getCommitted();
        this.nonHeapMax=nonHeap.getMax();




    }

    public long getHeapInit() {
        return heapInit;
    }

    public void setHeapInit(long heapInit) {
        this.heapInit = heapInit;
    }

    public long getHeapUsed() {
        return heapUsed;
    }

    public void setHeapUsed(long heapUsed) {
        this.heapUsed = heapUsed;
    }

    public long getHeapCommitted() {
        return heapCommitted;
    }

    public void setHeapCommitted(long heapCommitted) {
        this.heapCommitted = heapCommitted;
    }

    public long getHeapMax() {
        return heapMax;
    }

    public void setHeapMax(long heapMax) {
        this.heapMax = heapMax;
    }

    public long getNonHeapInit() {
        return nonHeapInit;
    }

    public void setNonHeapInit(long nonHeapInit) {
        this.nonHeapInit = nonHeapInit;
    }

    public long getNonHeapUsed() {
        return nonHeapUsed;
    }

    public void setNonHeapUsed(long nonHeapUsed) {
        this.nonHeapUsed = nonHeapUsed;
    }

    public long getNonHeapCommitted() {
        return nonHeapCommitted;
    }

    public void setNonHeapCommitted(long nonHeapCommitted) {
        this.nonHeapCommitted = nonHeapCommitted;
    }

    public long getNonHeapMax() {
        return nonHeapMax;
    }

    public void setNonHeapMax(long nonHeapMax) {
        this.nonHeapMax = nonHeapMax;
    }



    /**
     * Returns a descriptive representation of this memory usage.
     */
    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("heapinit = " + heapInit + "(" + (heapInit >> 10) + "K) ");
        buf.append("heapused = " + heapUsed + "(" + (heapUsed >> 10) + "K) ");
        buf.append("heapcommitted = " + heapCommitted + "(" +
                (heapCommitted >> 10) + "K) " );
        buf.append("heapmax = " + heapMax + "(" + (heapMax >> 10) + "K)");


        buf.append("nonHeapinit = " + nonHeapInit + "(" + (nonHeapInit >> 10) + "K) ");
        buf.append("nonHeapused = " + nonHeapUsed + "(" + (nonHeapUsed >> 10) + "K) ");
        buf.append("nonHeapcommitted = " + nonHeapCommitted + "(" +
                (nonHeapCommitted >> 10) + "K) " );
        buf.append("nonHeapmax = " + nonHeapMax + "(" + (nonHeapMax >> 10) + "K)");





        return buf.toString();
    }
}
