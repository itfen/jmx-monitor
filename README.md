# jmx-monitor

#### 介绍
用于连接Java.exe 进程，获取MXBean，可以用于监控JVM中的进程数量和堆内存使用情况

#### 目录说明
```

└─com
    └─unitfen
        └─jmx
            ├─client
            │  ├─entity
            │  └─impl
            └─mxbean
                └─creator
```


client目录是存放Jmx连接相关的一些文件  
 entity 是用于返回MXBean时返回数据的实体类  
 impl 是放服务类的  
 mxbean.creator 是用于获取MXBean时的一些策略类



#### 安装教程

这是一个jar包，依赖于jdk中的tool.jar 
使用IDE开发工具 打包成jar包即可 




#### 使用说明
先导入本jar包到目标工程中

使用方法可以参照com.unitfen.jmx.Main 中main方法的测试代码 

```


               MXBeanServiceTest mxBeanService= new MXBeanServiceTest(pid);
                System.out.println(mxBeanService.getMemoryUsageCount());
                System.out.println(mxBeanService.getThteadCount());

```

pid为本地的java.exe进程号
com.unitfen.jmx.client.impl.MXBeanServiceTest是写的样例  
如果需要扩展的话可以参考该类。


#### 扩展说明

1.先定义对应的MxBean类 
这个是java 中和jmx服务的基本操作 ，
可以参照 java.lang.management.MemoryMXBean


2.再定义MxBean类的创建策略，取名为*Creator 

本质上就是将MXBean类和Creator绑定了，作为一种策略传参给jmxc.MXBeanCreator（）方法来获取MXBean.

 可以参照 com.unitfen.jmx.mxbean.creator的格式 如下 ：


```

public class MemoryMXBeanCreator extends MXBeanCreator{
    @Override
    public Class<?> getMXBeanClass() {
        return  MemoryMXBean.class; //第一步中定义的mxbean类
    }

    @Override
    String getMxbeanName() {
        return  "java.lang:type=Memory";//这个是mxbean类的重要参数ObjectName
    }
}
```

3.连接jmx和取得mxbean 

以获取进程信息为例 


```


 public ThreadEntity getThteadCount(){
        //先根据进程pid连接到jmx
        JMXConnectorService jmxc = JMXConnectorService.getInstance(pid);
        try {
        // 根据传入的第二步中定义的creator类 获取到Mxbean
            ThreadMXBean threadInfo = jmxc.MXBeanCreator(new ThreadMXBeanCreator());
        //最后是做了一个数据转为实体类的 （可忽略）
          return  new ThreadEntity(threadInfo);
        }catch (Exception es){
            return null;
        }
    }



```



 
